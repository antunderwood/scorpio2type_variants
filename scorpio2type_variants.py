import json
import glob
import git
import shutil
import os

# clone constellations repo
if os.path.exists("constellations"):
    shutil.rmtree("constellations")
git.Git().clone("https://github.com/cov-lineages/constellations.git")

#  make set for sites
sites = set()

#  loop through each json file in the constellatiosns repo
for definition_file in glob.glob("constellations/constellations/definitions/*.json"):
    # load json as python object
    with open(definition_file) as infile:
        definition = json.load(infile)
        # add site to set depending on site type: del, nuc, or an amino acid position
        for site in definition['sites']:
            if site.startswith("nuc"):
                sites.add(site.replace("nuc", "snp"))
            elif site.startswith("del"):
                 sites.add(site)
            else:
                sites.add(f"aa:{site.upper()}")

# write sites to file
with open("type_variants.config", "w") as outfile:
    for site in sorted(sites, key=str.lower):
        outfile.write(f"{site}\n")

